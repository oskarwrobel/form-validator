# Form validator 

## Note:
Module is under development, currently there is available only 2 validators (`required` and `email`).

## About:
Form validator giving nice user experience. Form fields doesn't become invalid during first typing.
Most of popular form validators doesn't give possibility for users to avoid "making mistakes".
E.g. user starts to fill in input witch email address and after writing first character input become invalid,
because provided email address doesn't match to email pattern.

## How to use:
html:

```html
<form>
	<fieldset> <!-- Wrapper for every form field is required -->
		<label for="company">Company name:</label>
		<input type="text" id="company" name="companyName">
	</fieldset>
	<fieldset> <!-- Wrapper for every form field is required -->
		<label for="company">Company email address:</label>
		<input type="email" id="email" name="companyEmail">
	</fieldset>
</form>
```

javascript:

```javascript
import FormValidator from 'path/to/form-validator.js'

const formValidator = new FormValidator( document.querySelector( 'form' ), {
	rules: {
		companyName { // value of form field name
			required: 'You need to provide company name.'
		},
		companyEmail: {
			required: 'You need to provide company email address.',
			email: 'Invalid email address.'
		}
	},
	success: ( event ) => {
		// Stop default form behaviour.
		event.preventDefault();

		// ... getting data from form and send.

		// Reset form at the end.
		formValidator.reset();
	}
} );
```
