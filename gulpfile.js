'use strict';

const path = require( 'path' );
const gulp = require( 'gulp' );
const rollup = require( 'rollup' ).rollup;
const rollupBabel = require( 'rollup-plugin-babel' );
const Server = require( 'karma' ).Server;

const src = './src';
const dist = './dist';

const utils = {
	bundle( options = {} ) {
		return rollup( {
			entry: path.join( src, 'form-validator.js' ),
			plugins: [
				rollupBabel( {
					presets: [ 'es2015-rollup' ]
				} )
			]
		} ).then( ( bundle ) => {
			bundle.write( {
				format: options.format,
				moduleName: 'FormValidator',
				dest: options.dest
			} )
		} ).catch( ( err ) => console.log( err.stack ) );
	}
};

const tasks = {
	build() {
		return Promise.all( [
			utils.bundle( {
				format: 'es6',
				dest: path.join( dist, 'form-validator.js' )
			} ),
			utils.bundle( {
				format: 'iife',
				dest: path.join( dist, 'form-validator.es5.js' )
			} )
		] );
	},

	test( done ) {
		new Server( {
			configFile: __dirname + '/karma.conf.js'
		}, done ).start();
	}
};

gulp.task( 'test', tasks.test );
gulp.task( 'build', tasks.build );
