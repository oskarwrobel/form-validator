import FormField from '../src/form-field.js';

describe( 'FormField:', () => {
	let sandbox, emitterMock;

	beforeEach( () => {
		document.body.innerHTML = '<input type="text" name="test">';
		sandbox = sinon.sandbox.create();
		emitterMock = {
			emit: sandbox.spy()
		}
	} );

	afterEach( () => {
		sandbox.restore();
	} );

	describe( 'constructor()', () => {
		let inputEl;

		beforeEach( () => {
			inputEl = document.querySelector( 'input' );
		} );

		it( 'should create instance with properties', () => {
			const rules = { foo: 'bar' };
			const formField = new FormField( inputEl, emitterMock, rules );

			expect( formField ).to.have.property( 'isValid' ).to.equal( true );
		} );
	} );
} );
