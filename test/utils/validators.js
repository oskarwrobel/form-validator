import * as validators from '../../src/utils/validators.js';

/* global describe, it, beforeEach, expect */

describe( 'validators:', () => {
	describe( 'required', () => {
		it( 'should return true for non empty string', () => {
			expect( validators.required( 'Some string' ) ).to.be.true;
		} );

		it( 'should return false for empty string', () => {
			[
				'',
				' '
			].forEach( ( string ) => expect( validators.required( string ) ).to.be.false );
		} );
	} );

	describe( 'email', () => {
		it( 'should return true for valid email', () => {
			[
				'email@domain.com',
				'firstname.lastname@domain.com',
				'email@subdomain.domain.com',
				'firstname+lastname@domain.com',
				'"email"@domain.com',
				'1234567890@domain.com',
				'email@domain-one.com',
				'_______@domain.com',
				'email@domain.name',
				'email@domain.co.jp',
				'firstname-lastname@domain.com'
			].forEach( ( email ) => expect( validators.email( email ) ).to.be.true );
		} );

		it( 'should return false for invalid email', () => {
			[
				'plainaddress',
				'#@%^%#$@#$@#.com',
				'@domain.com',
				'Joe Smith <email@domain.com>',
				'email.domain.com',
				'email@domain@domain.com',
				'.email@domain.com',
				'email.@domain.com',
				'email..email@domain.com',
				'email@domain.com (Joe Smith)',
				'email@domain',
				'email@111.222.333.44444',
				'email@domain..com'
			].forEach( ( email ) => expect( validators.email( email ) ).to.be.false );
		} );
	} );
} );
