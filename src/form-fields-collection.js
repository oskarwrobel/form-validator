import EmitterMixin from './utils/emitter-mixin.js';
import FormField from './form-field.js';

/**
 * @memberOf FormValidator
 */
export default class FormFieldsCollection {
	/**
	 * @param {HTMLCollection} elements Form fields.
	 * @param {Object} rules Validation configuration.
	 */
	constructor( elements, rules ) {
		/**
		 * @type {EventsEmitter}
		 */
		this.emitter = Object.create( EmitterMixin );

		/**
		 * @private
		 * @type {Array.<FormField>}
		 */
		this._elements = [];

		// Create collection of each validable element.
		[].forEach.call( elements, ( element ) => {
			if ( isElementValidable( element ) ) {
				this.add( element, rules[ element.name ] );
			}
		} );
	}

	/**
	 * Check if each validable form field is valid.
	 *
	 * @returns {Boolean}
	 */
	get isValid() {
		return this._elements.every( ( element ) => element.isValid );
	}

	/**
	 * Add form field to collection.
	 *
	 * @param {FormValidator.FormField} element FormField instance.
	 * @param {Object} rules Validation configuration.
	 */
	add( element, rules ) {
		if ( !isElementValidable( element ) ) {
			throw new TypeError( 'Element is nod validable.' );
		}

		this._elements.push( new FormField( element, this.emitter, rules ) );
	}

	/**
	 * Validate each form field.
	 *
	 * @returns {Boolean}
	 */
	validate() {
		let isValid = true;

		this._elements.forEach( ( element ) => {
			if ( element.isValidable && !element.validate() ) {
				isValid = false;
			}
		} );

		return isValid;
	}

	/**
	 * Reset each form field.
	 */
	reset() {
		this._elements.forEach( ( element ) => element.reset() );
	}

	/**
	 * Disable each form field.
	 */
	disable() {
		this._elements.forEach( ( element ) => element.disabled = true );
	}

	/**
	 * Enable each form field.
	 */
	enable() {
		this._elements.forEach( ( element ) => element.disabled = false );
	}
}

/**
 * Checks if form field is validable.
 *
 * @param {HTMLElement} element
 * @returns {boolean}
 */
function isElementValidable( element ) {
	return element instanceof HTMLInputElement ||
		element instanceof HTMLSelectElement ||
		element instanceof HTMLTextAreaElement;
}
