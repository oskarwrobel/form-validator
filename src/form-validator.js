import FormFieldsCollection from './form-fields-collection.js';

/**
 * Form validator.
 *
 * @type FormValidator
 */
export default class FormValidator {
	/**
	 * @param {HTMLFormElement} form Form node.
	 * @param {Object} [config={}] Validator configuration.
	 * @param {Object} [config.rules] Configuration rules
	 * 		{
	 *			formElementName: {
	 *				validator: 'Error message'
	 *			}
	 *		},
	 * 		{
	 *			email: {
	 *				email: 'Email is required.'
	 *				required: 'Email is invalid.',
	 *			}
	 *		}
	 * @param {Function} [config.success] Success callback.
	 */
	constructor( form, config = {} ) {
		/**
		 * Form element.
		 *
		 * @private
		 * @type {HTMLFormElement}
		 */
		this._formEl = form;

		/**
		 * Collection of form fields wrapped by {@link FormValidator.FormField}.
		 *
		 * @private
		 */
		this._formFields = new FormFieldsCollection( form.elements, config.rules );

		/**
		 * Configuration.
		 *
		 * @private
		 * @type {Object}
		 */
		this._config = config;

		/**
		 * Reference to submit button. For optimization purpose.
		 *
		 * @private
		 * @type {HTMLElement}
		 */
		this._submitButton = form.querySelector('[type=submit]');

		// Remove default HTML validation.
		this._formEl.setAttribute( 'novalidate', 'novalidate' );

		// Handler for submit event.
		this._formEl.addEventListener( 'submit', this._handleSubmit.bind( this ) );

		// Handler for validation event.
		// If form is invalid then disable submit button, or enable if form is valid.
		this._formFields.emitter.on( 'validate', () => this._toggleDisableOnSubmit( !this._formFields.isValid ) );
	}

	/**
	 * Handle submit event on form element and validate all defined fields.
	 * If form is invalid then preventDefault submit event.
	 *
	 * @private
	 * @param {Event} event Submit event.
	 */
	_handleSubmit( event ) {
		const isValid = this._formFields.validate();

		if ( isValid ) {
			if ( typeof this._config.success == 'function' ) {
				this._config.success( event );
			}
		} else {
			event.preventDefault();
			event.stopPropagation();
			return false;
		}
	}

	/**
	 * Disable or enable form submit button.
	 *
	 * @private
	 * @param {Boolean} isDisabled
	 */
	_toggleDisableOnSubmit( isDisabled ) {
		this._submitButton.disabled = isDisabled;
	}

	/**
	 * Disable each form field and submit button.
	 */
	disable() {
		this._formFields.disable();
		this._toggleDisableOnSubmit( true );
	}

	/**
	 * Enable each form field and submit button.
	 */
	enable() {
		this._formFields.enable();
		this._toggleDisableOnSubmit( false );
	}

	/**
	 * Reset each form field.
	 */
	reset() {
		this._formEl.reset();
		this._formFields.reset();
		this.enable();
	}
}
