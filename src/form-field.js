import * as validators from './utils/validators.js';

const errorClass = 'error';

/**
 * Wrapper for each form element.
 *
 * @memberOf FormValidator
 */
export default class FormField {
	constructor( element, collectionEmitter, rules = null ) {
		/**
		 * Is field valid or invalid.
		 *
		 * @type {boolean}
		 */
		this.isValid = true;

		/**
		 * Reference to the DOM representation of this field.
		 *
		 * @private
		 * @type {HTMLElement}
		 */
		this._element = element;

		/**
		 * Is field touched by user or validation.
		 *
		 * @private
		 * @type {Boolean}
		 */
		this._isDirty = false;

		/**
		 * Validation rules.
		 *
		 * @private
		 * @type {Object}
		 */
		this._rules = rules;

		/**
		 * Emitter instance of {@link FormValidator.FormFieldsCollection}.
		 *
		 * @private
		 * @type {EventsEmitter}
		 */
		this._collectionEmitter = collectionEmitter;

		/**
		 * Message DOM element.
		 *
		 * @private
		 * @type {null|HTMLElement}
		 */
		this._messageElement = null;

		/**
		 * References to events listeners callbacks for `removeEventListener()`.
		 *
		 * @private
		 * @type {Object}
		 */
		this._eventRefs = {};

		// Attach blur event on element to detect when element become dirty.
		this._attachBlur();
	}

	/**
	 * Check if element is validable - has at east one defined validation rule.
	 *
	 * @returns {Boolean}
	 */
	get isValidable() {
		return !!this._rules;
	}

	/**
	 * Disable or enable field.
	 *
	 * @param {Boolean} isDisabled
     */
	set disabled( isDisabled ) {
		this._element.disabled = !!isDisabled;
	}

	/**
	 * Attach blur event to {FormValidator#_element}.
	 *
	 * @private
	 */
	_attachBlur() {
		if ( !this.isValidable ) {
			return;
		}

		this._eventRefs._handleBlur = () => this._handleBlur();
		this._element.addEventListener( 'blur', this._eventRefs._handleBlur );
	}

	/**
	 * Validate field on blur which also set field state as dirty.
	 * For better user experience form field is not validating during first typing (when is not dirty).
	 *
	 * @private
	 */
	_handleBlur() {
		this._element.removeEventListener( 'blur', this._eventRefs._handleBlur );
		this.validate();
	}

	/**
	 * Mark field as dirty and attach validation on `input`.
	 * For better user experience form field is not validate during first typing (when is not dirty).
	 *
	 * @private
	 */
	_setDirty() {
		if ( this._isDirty || !this.isValidable ) {
			return;
		}

		this._eventRefs.validate = () => this.validate();
		this._element.addEventListener( 'input', this._eventRefs.validate );
		this._isDirty = true;
	}

	/**
	 * Clear dirty state from form field and remove validation on input event.
	 *
	 * @private
	 */
	_clearDirty() {
		if ( !this._isDirty || !this.isValidable ) {
			return;
		}

		this._element.removeEventListener( 'input', this._eventRefs.validate );
		this._isDirty = false;
	}

	/**
	 * Validate form field by each rule defined for this form.
	 * Order of defined rules matters! First error stops validation for other rules.
	 *
	 * @returns {Boolean}
	 */
	validate() {
		if ( !this.isValidable ) {
			return true;
		}

		let error = '';

		// Validates by rules order.
		// Breaks loop after finding first invalid rule.
		this.isValid = Object.keys( this._rules ).every( ( rule ) => {
			if ( !validators[ rule ]( this._element.value ) ) {
				error = this._rules[ rule ];
				return false;
			}

			return true;
		} );

		this._setDirty();
		this._toggleMessage( error );
		this._collectionEmitter.emit( 'validate' );

		return this.isValid;
	}

	/**
	 * Mark field as disable.
	 */
	disable() {
		this._element.disabled = true;
	}

	/**
	 * Mark field as enable.
	 */
	enable() {
		this._element.disabled = false;
	}

	/**
	 * Reset field value and clear dirty state.
	 *
	 * @protected
	 */
	reset() {
		this._clearDirty();
		this._attachBlur();
	}

	/**
	 * Toggle form field error message. If error message is empty then hide message element or show if error message
	 * is defined.
	 *
	 * @private
	 * @param {String} error Error message
	 */
	_toggleMessage( error ) {
		if ( error ) {
			this._showErrorMessage( error );
		} else {
			this._clearErrorMessage();
		}
	}

	/**
	 * Create if not exists and show node with error message. Node is append just after form element.
	 *
	 * @private
	 * @param {String} errorMessage
	 */
	_showErrorMessage( errorMessage ) {
		const parent = this._element.parentNode;

		if ( !this._messageElement ) {
			this._messageElement = document.createElement( 'span' );
			parent.appendChild( this._messageElement );
		}

		this._messageElement.textContent = errorMessage;
		parent.classList.add( errorClass );
	}

	/**
	 * clear and hide node with error message.
	 *
	 * @private
	 */
	_clearErrorMessage() {
		if ( !this._messageElement ) {
			return;
		}

		this._element.parentNode.classList.remove( errorClass );
		this._messageElement.textContent = '';
	}
}
