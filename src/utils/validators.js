/**
 * Check if value is not empty.
 *
 * @param {String} value
 * @returns {Boolean}
 */
export function required( value ) {
	return value.trim() !== '';
}

/**
 * Check if passed value is a valid email pattern.
 *
 * @param {String} value
 * @returns {Boolean}
 */
export function email( value ) {
	const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test( value );
}
