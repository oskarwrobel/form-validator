var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj;
};

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/**
 * Mixin that injects the events API into its host.
 *
 * @mixin FormValidator.EmitterMixin
 * @memberOf FormValidator
 */
var EmitterMixin = {
	/**
  * Store for attached events witch callbacks.
  *
  * @private
  */
	_events: [],

	/**
  * Registers a callback function to be executed when an event is emitted.
  *
  * @method utils.EmitterMixin#on
  * @param {String} event The name of the event.
  * @param {Function} callback The function to be called on event.
  */
	on: function on(event, callback) {
		if (!Array.isArray(this._events[event])) {
			this._events[event] = [];
		}

		this._events[event].push(callback);
	},


	/**
  * Stops executing the callback on the given event.
  *
  * @method utils.EmitterMixin#off
  * @param {String} event The name of the event.
  * @param {Function} callback The function to stop being called.
  */
	off: function off(event, callback) {
		if (_typeof(this._events[event]) === 'object') {
			var index = this._events[event].indexOf(callback);

			if (index > -1) {
				this._events[event].splice(index, 1);
			}
		}
	},


	/**
  * Fires an event, executing all callbacks registered for it.
  *
  * @method utils.EmitterMixin#emit
  * @param {String} event The name of the event.
  * @param {...*} [args] Additional arguments to be passed to the callbacks.
  */
	emit: function emit(event) {
		for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
			args[_key - 1] = arguments[_key];
		}

		if (_typeof(this._events[event]) === 'object') {
			var callbacks = this._events[event].slice();
			var length = callbacks.length;

			args = [].slice.call(arguments, 1);

			for (var i = 0; i < length; i++) {
				callbacks[i].apply(this, args);
			}
		}
	}
};

/**
 * Check if value is not empty.
 *
 * @param {String} value
 * @returns {Boolean}
 */
function required(value) {
  return value.trim() !== '';
}

/**
 * Check if passed value is a valid email pattern.
 *
 * @param {String} value
 * @returns {Boolean}
 */
function email(value) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(value);
}

var validators = Object.freeze({
  required: required,
  email: email
});

var errorClass = 'error';

/**
 * Wrapper for each form element.
 *
 * @memberOf FormValidator
 */

var FormField = function () {
	function FormField(element, collectionEmitter) {
		var rules = arguments.length <= 2 || arguments[2] === undefined ? null : arguments[2];
		classCallCheck(this, FormField);

		/**
   * Is field valid or invalid.
   *
   * @type {boolean}
   */
		this.isValid = true;

		/**
   * Reference to the DOM representation of this field.
   *
   * @private
   * @type {HTMLElement}
   */
		this._element = element;

		/**
   * Is field touched by user or validation.
   *
   * @private
   * @type {Boolean}
   */
		this._isDirty = false;

		/**
   * Validation rules.
   *
   * @private
   * @type {Object}
   */
		this._rules = rules;

		/**
   * Emitter instance of {@link FormValidator.FormFieldsCollection}.
   *
   * @private
   * @type {EventsEmitter}
   */
		this._collectionEmitter = collectionEmitter;

		/**
   * Message DOM element.
   *
   * @private
   * @type {null|HTMLElement}
   */
		this._messageElement = null;

		/**
   * References to events listeners callbacks for `removeEventListener()`.
   *
   * @private
   * @type {Object}
   */
		this._eventRefs = {};

		// Attach blur event on element to detect when element become dirty.
		this._attachBlur();
	}

	/**
  * Check if element is validable - has at east one defined validation rule.
  *
  * @returns {Boolean}
  */


	createClass(FormField, [{
		key: '_attachBlur',


		/**
   * Attach blur event to {FormValidator#_element}.
   *
   * @private
   */
		value: function _attachBlur() {
			var _this = this;

			if (!this.isValidable) {
				return;
			}

			this._eventRefs._handleBlur = function () {
				return _this._handleBlur();
			};
			this._element.addEventListener('blur', this._eventRefs._handleBlur);
		}

		/**
   * Validate field on blur which also set field state as dirty.
   * For better user experience form field is not validating during first typing (when is not dirty).
   *
   * @private
   */

	}, {
		key: '_handleBlur',
		value: function _handleBlur() {
			this._element.removeEventListener('blur', this._eventRefs._handleBlur);
			this.validate();
		}

		/**
   * Mark field as dirty and attach validation on `input`.
   * For better user experience form field is not validate during first typing (when is not dirty).
   *
   * @private
   */

	}, {
		key: '_setDirty',
		value: function _setDirty() {
			var _this2 = this;

			if (this._isDirty || !this.isValidable) {
				return;
			}

			this._eventRefs.validate = function () {
				return _this2.validate();
			};
			this._element.addEventListener('input', this._eventRefs.validate);
			this._isDirty = true;
		}

		/**
   * Clear dirty state from form field and remove validation on input event.
   *
   * @private
   */

	}, {
		key: '_clearDirty',
		value: function _clearDirty() {
			if (!this._isDirty || !this.isValidable) {
				return;
			}

			this._element.removeEventListener('input', this._eventRefs.validate);
			this._isDirty = false;
		}

		/**
   * Validate form field by each rule defined for this form.
   * Order of defined rules matters! First error stops validation for other rules.
   *
   * @returns {Boolean}
   */

	}, {
		key: 'validate',
		value: function validate() {
			var _this3 = this;

			if (!this.isValidable) {
				return true;
			}

			var error = '';

			// Validates by rules order.
			// Breaks loop after finding first invalid rule.
			this.isValid = Object.keys(this._rules).every(function (rule) {
				if (!validators[rule](_this3._element.value)) {
					error = _this3._rules[rule];
					return false;
				}

				return true;
			});

			this._setDirty();
			this._toggleMessage(error);
			this._collectionEmitter.emit('validate');

			return this.isValid;
		}

		/**
   * Mark field as disable.
   */

	}, {
		key: 'disable',
		value: function disable() {
			this._element.disabled = true;
		}

		/**
   * Mark field as enable.
   */

	}, {
		key: 'enable',
		value: function enable() {
			this._element.disabled = false;
		}

		/**
   * Reset field value and clear dirty state.
   *
   * @protected
   */

	}, {
		key: 'reset',
		value: function reset() {
			this._clearDirty();
			this._attachBlur();
		}

		/**
   * Toggle form field error message. If error message is empty then hide message element or show if error message
   * is defined.
   *
   * @private
   * @param {String} error Error message
   */

	}, {
		key: '_toggleMessage',
		value: function _toggleMessage(error) {
			if (error) {
				this._showErrorMessage(error);
			} else {
				this._clearErrorMessage();
			}
		}

		/**
   * Create if not exists and show node with error message. Node is append just after form element.
   *
   * @private
   * @param {String} errorMessage
   */

	}, {
		key: '_showErrorMessage',
		value: function _showErrorMessage(errorMessage) {
			var parent = this._element.parentNode;

			if (!this._messageElement) {
				this._messageElement = document.createElement('span');
				parent.appendChild(this._messageElement);
			}

			this._messageElement.textContent = errorMessage;
			parent.classList.add(errorClass);
		}

		/**
   * clear and hide node with error message.
   *
   * @private
   */

	}, {
		key: '_clearErrorMessage',
		value: function _clearErrorMessage() {
			if (!this._messageElement) {
				return;
			}

			this._element.parentNode.classList.remove(errorClass);
			this._messageElement.textContent = '';
		}
	}, {
		key: 'isValidable',
		get: function get() {
			return !!this._rules;
		}

		/**
   * Disable or enable field.
   *
   * @param {Boolean} isDisabled
      */

	}, {
		key: 'disabled',
		set: function set(isDisabled) {
			this._element.disabled = !!isDisabled;
		}
	}]);
	return FormField;
}();

/**
 * @memberOf FormValidator
 */

var FormFieldsCollection = function () {
	/**
  * @param {HTMLCollection} elements Form fields.
  * @param {Object} rules Validation configuration.
  */

	function FormFieldsCollection(elements, rules) {
		var _this = this;

		classCallCheck(this, FormFieldsCollection);

		/**
   * @type {EventsEmitter}
   */
		this.emitter = Object.create(EmitterMixin);

		/**
   * @private
   * @type {Array.<FormField>}
   */
		this._elements = [];

		// Create collection of each validable element.
		[].forEach.call(elements, function (element) {
			if (isElementValidable(element)) {
				_this.add(element, rules[element.name]);
			}
		});
	}

	/**
  * Check if each validable form field is valid.
  *
  * @returns {Boolean}
  */


	createClass(FormFieldsCollection, [{
		key: 'add',


		/**
   * Add form field to collection.
   *
   * @param {FormValidator.FormField} element FormField instance.
   * @param {Object} rules Validation configuration.
   */
		value: function add(element, rules) {
			if (!isElementValidable(element)) {
				throw new TypeError('Element is nod validable.');
			}

			this._elements.push(new FormField(element, this.emitter, rules));
		}

		/**
   * Validate each form field.
   *
   * @returns {Boolean}
   */

	}, {
		key: 'validate',
		value: function validate() {
			var isValid = true;

			this._elements.forEach(function (element) {
				if (element.isValidable && !element.validate()) {
					isValid = false;
				}
			});

			return isValid;
		}

		/**
   * Reset each form field.
   */

	}, {
		key: 'reset',
		value: function reset() {
			this._elements.forEach(function (element) {
				return element.reset();
			});
		}

		/**
   * Disable each form field.
   */

	}, {
		key: 'disable',
		value: function disable() {
			this._elements.forEach(function (element) {
				return element.disabled = true;
			});
		}

		/**
   * Enable each form field.
   */

	}, {
		key: 'enable',
		value: function enable() {
			this._elements.forEach(function (element) {
				return element.disabled = false;
			});
		}
	}, {
		key: 'isValid',
		get: function get() {
			return this._elements.every(function (element) {
				return element.isValid;
			});
		}
	}]);
	return FormFieldsCollection;
}();

function isElementValidable(element) {
	return element instanceof HTMLInputElement || element instanceof HTMLSelectElement || element instanceof HTMLTextAreaElement;
}

/**
 * Form validator.
 *
 * @type FormValidator
 */

var FormValidator = function () {
	/**
  * @param {HTMLFormElement} form Form node.
  * @param {Object} [config={}] Validator configuration.
  * @param {Object} [config.rules] Configuration rules
  * 		{
  *			formElementName: {
  *				validator: 'Error message'
  *			}
  *		},
  * 		{
  *			email: {
  *				email: 'Email is required.'
  *				required: 'Email is invalid.',
  *			}
  *		}
  * @param {Function} [config.success] Success callback.
  */

	function FormValidator(form) {
		var _this = this;

		var config = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
		classCallCheck(this, FormValidator);

		/**
   * Form element.
   *
   * @private
   * @type {HTMLFormElement}
   */
		this._formEl = form;

		/**
   * Collection of form fields wrapped by {@link FormValidator.FormField}.
   *
   * @private
   */
		this._formFields = new FormFieldsCollection(form.elements, config.rules);

		/**
   * Configuration.
   *
   * @private
   * @type {Object}
   */
		this._config = config;

		/**
   * Reference to submit button. For optimization purpose.
   *
   * @private
   * @type {HTMLElement}
   */
		this._submitButton = form.querySelector('[type=submit]');

		// Remove default HTML validation.
		this._formEl.setAttribute('novalidate', 'novalidate');

		// Handler for submit event.
		this._formEl.addEventListener('submit', this._handleSubmit.bind(this));

		// Handler for validation event.
		// If form is invalid then disable submit button, or enable if form is valid.
		this._formFields.emitter.on('validate', function () {
			return _this._toggleDisableOnSubmit(!_this._formFields.isValid);
		});
	}

	/**
  * Handle submit event on form element and validate all defined fields.
  * If form is invalid then preventDefault submit event.
  *
  * @private
  * @param {Event} event Submit event.
  */


	createClass(FormValidator, [{
		key: '_handleSubmit',
		value: function _handleSubmit(event) {
			var isValid = this._formFields.validate();

			if (isValid) {
				if (typeof this._config.success == 'function') {
					this._config.success(event);
				}
			} else {
				event.preventDefault();
				event.stopPropagation();
				return false;
			}
		}

		/**
   * Disable or enable form submit button.
   *
   * @private
   * @param {Boolean} isDisabled
   */

	}, {
		key: '_toggleDisableOnSubmit',
		value: function _toggleDisableOnSubmit(isDisabled) {
			this._submitButton.disabled = isDisabled;
		}

		/**
   * Disable each form field and submit button.
   */

	}, {
		key: 'disable',
		value: function disable() {
			this._formFields.disable();
			this._toggleDisableOnSubmit(true);
		}

		/**
   * Enable each form field and submit button.
   */

	}, {
		key: 'enable',
		value: function enable() {
			this._formFields.enable();
			this._toggleDisableOnSubmit(false);
		}

		/**
   * Reset each form field.
   */

	}, {
		key: 'reset',
		value: function reset() {
			this._formEl.reset();
			this._formFields.reset();
			this.enable();
		}
	}]);
	return FormValidator;
}();

export default FormValidator;